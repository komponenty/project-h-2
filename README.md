# Epub Manager 1.0
# Autorzy: Artur Pasierb, Patryk Nowak, Mateusz Marcysiak
# Windows: `run.bat` lub `gradlew run`
# Linux: `chmod +x run.sh && ./run.sh` lub `chmod +x gradlew && ./gradlew run`
#
#[Dokumentacja](https://docs.google.com/document/d/19X-ECJanQJqmGnfcU1AOy-sd1Do4b2zXGisM0GOXb_I/)
#
# Pliki epub można pobrać z [wolnelektury.pl](http://wolnelektury.pl/)