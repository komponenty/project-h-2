package pk.epub.Reader.Contracts;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;

/**
 *
 * @author Patryk
 */
public interface IReader {

    //wydobycie informacji
    public List<String> getPages();
    public void setCounter(int counter);
    public int getCounter();
    public String getContainer();
    
    public void Read(String path) throws FileNotFoundException, IOException, ParserConfigurationException, SAXException, XPathExpressionException;
    
    //przewijanie
    public File PreviousPage(String Directory);
    public File NextPage(String Directory);
    public File Beginning(String Directory);
    public File Ending(String Directory);
    
    //serializacja
    public void saveCounter(String Directory) throws FileNotFoundException;
    public int getCounter(String Directory) throws FileNotFoundException, IOException;
}
