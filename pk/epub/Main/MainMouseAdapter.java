package pk.epub.Main;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;
import pk.epub.Main.Contracts.IMain;
import pk.epub.Reader.Contracts.IReader;

/**
 *
 * @author Artur
 */
public class MainMouseAdapter extends MouseAdapter {

    private final IMain main;
    
    public MainMouseAdapter(IMain main) {
        this.main = main;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getClickCount() == 2) {
            try {
                String[] epub = main.getLibrary().getSelectedEpub();
                IReader reader = main.getReader();
                reader.Read(epub[1] + "/META-INF/container.xml");      
                main.getReaderFrame().Display(epub[1] + "/", reader);
            } catch (IOException ex) {
                Logger.getLogger(MainMouseAdapter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParserConfigurationException ex) {
                Logger.getLogger(MainMouseAdapter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(MainMouseAdapter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (XPathExpressionException ex) {
                Logger.getLogger(MainMouseAdapter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(MainMouseAdapter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
