package pk.epub.Main;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.xhtmlrenderer.simple.FSScrollPane;
import org.xhtmlrenderer.simple.XHTMLPanel;
import pk.epub.Reader.Contracts.IReader;

/**
 *
 * @author Patryk
 */
public class ReaderFrame {
    
     XHTMLPanel panel;

    public void Display(final String directory, final IReader reader) throws Exception {
        
        panel = new XHTMLPanel();
        final String Directory= directory+reader.getContainer();
                
        //wczytanie licznika
        reader.setCounter(reader.getCounter(Directory));
        
        panel.setDocument(new File(Directory + reader.getPages().get(reader.getCounter()).toString()));

        FSScrollPane scroll = new FSScrollPane(panel);

        //dodaje buttony
        JButton firstPage = new JButton("<<");
        JButton nextPage = new JButton(">");
        JButton previousPage = new JButton("<");
        JButton lastPage = new JButton(">>");
        
        
        //eventy
        previousPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.PreviousPage(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        nextPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.NextPage(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        //dwa nowe eventy dla << i >>
        
        firstPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.Beginning(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        lastPage.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.Ending(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        

        JPanel panel1 = new JPanel();
        panel1.setSize(50, 500);
        
        panel1.add(firstPage, BorderLayout.WEST);
        panel1.add(previousPage, BorderLayout.WEST);
        panel1.add(nextPage, BorderLayout.EAST);
        panel1.add(lastPage, BorderLayout.EAST);
        

        JFrame frame = new JFrame("Epub Reader");

        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.getContentPane().add(scroll);
        frame.pack();
        frame.setSize(600, 800);
        frame.setLocationRelativeTo(null);
        frame.add(panel1, BorderLayout.SOUTH);
        frame.setVisible(true);
    }
}
