package pk.epub.Main;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pk.epub.Creator.Contracts.ICreator;
import pk.epub.Library.Contracts.ILibrary;
import pk.epub.Main.Contracts.IMain;
import pk.epub.Main.Contracts.IMixin;
import pk.epub.Opener.Contracts.IOpener;
import pk.epub.Reader.Contracts.IReader;
import pk.h1.backup.BackupListener;
import pk.h1.backup.contracts.IBackup;

/**
 *
 * @author Artur
 */
public class Main implements IMain, ActionListener, ListSelectionListener, BackupListener {

    public Main(ICreator creator, ILibrary library, IOpener opener, IReader reader, IBackup backup) {
        this.creator = creator;
        this.library = library;
        library.addSelectionListener(this);
        library.addMouseListener(new MainMouseAdapter(this));
        this.opener = opener;
        this.reader = reader;
        this.backup = backup;
        backup.addListener(this);

        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new EpubFilter());
        mainFrame = new MainView(this);
        creatorDialog = new CreatorView(this);
        readerFrame = new ReaderFrame();
    }

    private final ICreator creator;
    private final ILibrary library;
    private final IOpener opener;
    private final IReader reader;
    private final IBackup backup;

    private final MainView mainFrame;
    private final CreatorView creatorDialog;
    private final ReaderFrame readerFrame;
    private final JFileChooser fileChooser;

    @Override
    public ILibrary getLibrary() {
        return library;
    }

    @Override
    public ICreator getCreator() {
        return creator;
    }

    @Override
    public MainView getMainFrame() {
        return mainFrame;
    }
    
    @Override
    public IReader getReader() {
        return reader;
    }
    
    @Override
    public ReaderFrame getReaderFrame() {
        return readerFrame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton btn = (JButton) e.getSource();
        String[] epub = null;
        int result;
        switch (btn.getText()) {
            case "Import":
                result = fileChooser.showOpenDialog(mainFrame);
                String path = null;
                String dirpath = null;
                if (result == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    path = file.getAbsolutePath();
                    dirpath = file.getParentFile().getAbsolutePath();
                }

                if (path != null) {
                    String name = path.substring(dirpath.length() + 1);
                    name = name.substring(0, name.length() - 5);

                    //opener wypakowuje ksiazke w folderze /library, ktorej zrodlo jest z path, a nazwa nowego folderu zgodna z nazwa
                    opener.OpenEpub(library.getLibraryDir(), path, name);

                    library.addNewEpub(opener.getTitle(), library.getLibraryDir() + File.separator + name);
                }
                break;
            case "Eksport":
                epub = library.getSelectedEpub();
                if (library.getView().getjTable1().getRowCount() > library.getView().getjTable1().getSelectedRow()
                        && library.getView().getjTable1().getSelectedRow() != -1) {

                    result = fileChooser.showSaveDialog(mainFrame);
                    if (result == JFileChooser.APPROVE_OPTION) {
                        try {
                            File selectedFile = fileChooser.getSelectedFile();
                            String fileName = selectedFile.getCanonicalPath().toLowerCase();
                            if (!fileName.endsWith(".epub")) {
                                fileName += ".epub";
                            }
                            creator.getPackager().pack(epub[1], fileName);
                            creator.clear();
                        } catch (IOException ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                break;
            case "Czytaj":
                epub = library.getSelectedEpub();
                if (library.getView().getjTable1().getRowCount() > library.getView().getjTable1().getSelectedRow()
                        && library.getView().getjTable1().getSelectedRow() != -1) {
            try {                    
                reader.Read(epub[1] + "/META-INF/container.xml");
            } catch (    IOException | ParserConfigurationException | SAXException | XPathExpressionException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }


                    try {
                        readerFrame.Display(epub[1] + "/", reader);
                        //zapis tylko tutaj
                        //reader.saveLicznik(epub[1] + "/");
                    } catch (Exception ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            case "Utworz":
                ((IMixin)creator).secretEpub();
                creatorDialog.setVisible(true);
                break;
            case "Usun":
                epub = library.getSelectedEpub();
                File file = new File(epub[1]);
                if (file.exists()) {
                    creator.getPackager().deleteFile(epub[1]);
                }
                library.removeSelectedEpub();
                break;
            case "Backup":
                backup.getView().setVisible(true);
                break;
            case "Autorzy":
                JOptionPane.showMessageDialog(mainFrame, "Artur Pasierb\nPatryk Nowak\nMateusz Marcysiak", "Autorzy", JOptionPane.INFORMATION_MESSAGE);
                break;
        }

    }

    @Override
    public void createEPUB() throws IOException {
        creator.setDir(library.getLibraryDir());

        creator.setTitle(creatorDialog.getjTextField1().getText());
        creator.setLanguage(creatorDialog.getjTextField2().getText());
        creator.addIdentifier("isbn", creatorDialog.getjTextField3().getText());

        creator.setCover(creatorDialog.getCover());
        creator.setStyle(creatorDialog.getStyle());

        for (BufferedImage bufferedImage : creatorDialog.getImages()) {
            creator.addImage(bufferedImage);
        }

        for (String string : creatorDialog.getChapters()) {
            creator.addChapterBody(string);
        }
        
        for (InputStream inputStream : creatorDialog.getFonts()) {
            creator.addFont(inputStream);
        }

        creator.createEpub(true, false);

        for (InputStream inputStream : creatorDialog.getFonts()) {
            inputStream.close();
        }

        library.addNewEpub(creatorDialog.getjTextField1().getText(), library.getLibraryDir() + File.separator + creatorDialog.getjTextField1().getText());
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {

        String[] epub = library.getSelectedEpub();
        
        JTable table = library.getView().getjTable1();

        if (table.getSelectedRow() < table.getRowCount() && table.getSelectedRow() != -1) {
            try {
                String cover = findCover(epub[1]);
                if (!cover.equals(":no:cover:")) {
                    String pathCover = contentOPF.substring(0, contentOPF.lastIndexOf(File.separator));
                    String imgIco = epub[1] + File.separator + pathCover + File.separator + cover;
                    ImageIcon imageIcon = new ImageIcon(new ImageIcon(imgIco).getImage().getScaledInstance(200, 300, Image.SCALE_DEFAULT));
                    mainFrame.setCover(imageIcon);
                    mainFrame.setNameText(epub[0]);
                    mainFrame.setPathText(epub[1]);
                } else {
                    ImageIcon imageIcon = new ImageIcon(new ImageIcon("resources/nocover.jpg").getImage().getScaledInstance(200, 300, Image.SCALE_DEFAULT));
                    mainFrame.setCover(imageIcon);
                    mainFrame.setNameText(epub[0]);
                    mainFrame.setPathText(epub[1]);
                }
            } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            ImageIcon imageIcon = new ImageIcon(new ImageIcon("resources/logo.jpg").getImage().getScaledInstance(200, 300, Image.SCALE_DEFAULT));
            mainFrame.setCover(imageIcon);
            mainFrame.setNameText("");
            mainFrame.setPathText("");
        }

    }

    private String contentOPF;

    private String findCover(String path) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {

        File fXmlFile = new File(path + File.separator + "META-INF" + File.separator + "container.xml");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("rootfile");

        String container = null;

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;
                String value = eElement.getAttribute("full-path");
                container = value;
            }
        }

        contentOPF = container.replace("/", File.separator);

        String path2 = path + File.separator;
        path2 = path2 + contentOPF;

        File fXmlFile2 = new File(path2);
        DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
        doc = dBuilder.parse(fXmlFile2);

        XPath xPath = XPathFactory.newInstance().newXPath();
        doc.getDocumentElement().normalize();

        String exp = "package/metadata/meta[@name='cover']";

        Node node = (Node) xPath.compile(exp).evaluate(doc, XPathConstants.NODE);

        if (node != null) {
            NamedNodeMap attributes = node.getAttributes();
            Node namedItem2 = attributes.getNamedItem("content");

            String coverID = namedItem2.getNodeValue();

            exp = "package/manifest/item[@id='" + coverID + "']";

            Node node2 = (Node) xPath.compile(exp).evaluate(doc, XPathConstants.NODE);
            if (node2 != null) {
                NamedNodeMap attributes2 = node2.getAttributes();
                Node namedItem3 = attributes2.getNamedItem("href");
                return namedItem3.getNodeValue();
            } else {
                return ":no:cover:";
            }

        } else {
            return ":no:cover:";
        }
    }

    @Override
    public void refresh() {
        library.getModel().ref();
        library.getView().getjTable1().revalidate();
    }
}
