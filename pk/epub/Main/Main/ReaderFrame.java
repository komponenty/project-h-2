package pk.epub.Main;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.xhtmlrenderer.simple.FSScrollPane;
import org.xhtmlrenderer.simple.XHTMLPanel;
import pk.epub.Reader.Contracts.IReader;

/**
 *
 * @author Patryk
 */
public class ReaderFrame {
    
     XHTMLPanel panel;

    public void Display(final String directory, final IReader reader) throws Exception {
        
        panel = new XHTMLPanel();
        final String Directory= directory+reader.getContainer();
        
//        System.out.println("directory: " + Directory);
        
        //wczytanie licznika
        reader.setLicznik(reader.getLicznik(Directory));
//        System.out.println("Licznik :"+reader.getLicznik(Directory));
        
        panel.setDocument(new File(Directory + reader.getStrony().get(reader.getLicznik()).toString()));

        FSScrollPane scroll = new FSScrollPane(panel);

        //dodaje buttony
        JButton poczatek = new JButton("<<");
        JButton dalej = new JButton(">");
        JButton cofnij = new JButton("<");
        JButton koniec = new JButton(">>");
        
        
        //eventy
        cofnij.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.PreviousPage(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        dalej.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.NextPage(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        //dwa nowe eventy dla << i >>
        
        poczatek.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.Beginning(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        koniec.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    panel.setDocument(reader.Ending(Directory));
                } catch (Exception ex) {
                    Logger.getLogger(ReaderFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        

        JPanel panel1 = new JPanel();
        panel1.setSize(50, 500);
        
        panel1.add(poczatek, BorderLayout.WEST);
        panel1.add(cofnij, BorderLayout.WEST);
        panel1.add(dalej, BorderLayout.EAST);
        panel1.add(koniec, BorderLayout.EAST);
        

        JFrame frame = new JFrame("Epub Reader");

        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.getContentPane().add(scroll);
        frame.pack();
        frame.setSize(600, 800);
        frame.setLocationRelativeTo(null);
        frame.add(panel1, BorderLayout.SOUTH);
        frame.setVisible(true);
    }
}
