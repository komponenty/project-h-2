package pk.epub.Main;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import pk.epub.Main.Contracts.IMain;

/**
 *
 * @author Artur
 */
public class MainView extends JFrame {

    private final IMain main;

    private final String[] actions = new String[]{"Import", "Eksport", "Czytaj", "Utworz", "Usun", "Backup"};

    public String[] getActions() {
        return actions;
    }

    private final JLabel picLabel;
    private final JLabel name;
    private final JLabel path;

    public void setNameText(String name) {
        this.name.setText(name);
    }

    public void setPathText(String path) {
        this.path.setText(path);
    }

    public void setCover(ImageIcon ico) {
        this.picLabel.setIcon(ico);
    }

    public MainView(final IMain main) {
        super("Epub Manager");
        this.main = main;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setResizable(false);

        JPanel north = new JPanel();

        for (String string : actions) {
            JButton btn = new JButton(string);
            btn.addActionListener((ActionListener) this.main);
            north.add(btn);
        }
        
        JButton authors = new JButton("Autorzy");
        authors.addActionListener((ActionListener) this.main);
        north.add(authors);

        getContentPane().add(north, BorderLayout.NORTH);

        getContentPane().add(this.main.getLibrary().getView(), BorderLayout.CENTER);

        JPanel west = new JPanel(new BorderLayout());

        ImageIcon imageIcon = new ImageIcon(new ImageIcon("resources/logo.jpg").getImage().getScaledInstance(200, 300, Image.SCALE_DEFAULT));
        picLabel = new JLabel(imageIcon);
        west.add(picLabel, BorderLayout.NORTH);

        JPanel dataUnderCover = new JPanel(new GridLayout(0, 1));

        dataUnderCover.add(new JLabel("Tytul: "));
        name = new JLabel();
        dataUnderCover.add(name);

        dataUnderCover.add(new JLabel("Sciezka: "));
        path = new JLabel();
        dataUnderCover.add(path);
        
        west.add(dataUnderCover, BorderLayout.CENTER);

        getContentPane().add(west, BorderLayout.WEST);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

}
