package pk.epub.Main.Contracts;

import java.io.IOException;
import pk.epub.Creator.Contracts.ICreator;
import pk.epub.Library.Contracts.ILibrary;
import pk.epub.Main.MainView;
import pk.epub.Main.ReaderFrame;
import pk.epub.Reader.Contracts.IReader;

/**
 *
 * @author Artur
 */
public interface IMain {
    
    public ILibrary getLibrary();

    public ICreator getCreator();

    public MainView getMainFrame();
    
    public IReader getReader();
    
    public ReaderFrame getReaderFrame();
    
    public void createEPUB() throws IOException;
}
