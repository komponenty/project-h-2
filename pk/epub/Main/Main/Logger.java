package pk.epub.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author Artur
 */
public class Logger {
    
    private final File loggerFile;
    
    public Logger() {
        loggerFile = new File("log.txt");
        try {        
            loggerFile.createNewFile();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String currentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public void logOpen() {
        Writer out = null;
        try {
            out = new FileWriter(loggerFile, true);
            out.write(currentDateTime() + ": Zaimportowno plik epub." + System.lineSeparator());
            out.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);            
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void logPack() {
        Writer out = null;
        try {
            out = new FileWriter(loggerFile, true);
            out.write(currentDateTime() + ": Wyeksportowno plik epub" + System.lineSeparator());
            out.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);            
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void logRead() {
        Writer out = null;
        try {
            out = new FileWriter(loggerFile, true);
            out.write(currentDateTime() + ": Przeczytano plik epub." + System.lineSeparator());
            out.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);            
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void logCreate() {
        Writer out = null;
        try {
            out = new FileWriter(loggerFile, true);
            out.write(currentDateTime() + ": Stworzono plik epub." + System.lineSeparator());
            out.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);            
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    public void logDelete() {
        Writer out = null;
        try {
            out = new FileWriter(loggerFile, true);
            out.write(currentDateTime() + ": Skasowano plik epub." + System.lineSeparator());
            out.close();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);            
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }    
}
