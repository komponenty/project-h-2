package pk.epub.Creator;

import com.jamesmurty.utils.XMLBuilder;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import pk.epub.Creator.Contracts.ICreator;
import pk.epub.Creator.Contracts.IPackager;

/**
 *
 * @author Artur
 */
public class Creator implements ICreator {

    private IPackager packager;

    @Override
    public IPackager getPackager() {
        return packager;
    }

    public void setPackager(IPackager packager) {
        this.packager = packager;
    }

    private String dir;

    public String getDir() {
        return dir;
    }

    private String title;
    private String language;
    private Map<String, String> identifiers;

    private BufferedImage cover;
    private String style;

    private List<String> chapters;
    private List<BufferedImage> images;
    private List<InputStream> fonts;

    private String uid;

    public Creator() {
        this(new Packager(), "");
    }

    public Creator(String dir) {
        this(new Packager(), dir);
    }

    public Creator(IPackager packager) {
        this(packager, "");
    }

    public Creator(IPackager packager, String dir) {
        this.packager = packager;
        this.dir = dir + File.separator;

        title = null;
        language = null;
        identifiers = new HashMap<>();

        cover = null;
        style = null;

        chapters = new ArrayList<>();
        images = new ArrayList<>();
        fonts = new ArrayList<>();

        uid = null;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void addIdentifier(String type, String value) {
        this.identifiers.put(type, value);
    }

    @Override
    public void setCover(BufferedImage cover) {
        this.cover = cover;
    }

    @Override
    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public void addChapter(String content) {
        this.chapters.add(content);
    }

    @Override
    public void addImage(BufferedImage image) {
        this.images.add(image);
    }

    @Override
    public void createEpub() {
        createEpub(false, true);
    }

    private void writeMimetypeAndContainerFile() throws IOException, ParserConfigurationException, TransformerException {
        File dir = new File(this.dir + this.title + File.separator + "META-INF");
        dir.mkdirs();
        dir = new File(this.dir + this.title + File.separator + "OEBPS");
        dir.mkdir();
        packager.createFile(this.dir + this.title, "mimetype", "application/epub+zip");

        XMLBuilder builder = XMLBuilder.create("container", "urn:oasis:names:tc:opendocument:xmlns:container")
                .a("version", "1.0")
                .e("rootfiles")
                .e("rootfile")
                .a("full-path", "OEBPS/content.opf")
                .a("media-type", "application/oebps-package+xml");

        Properties prop = new Properties();
        prop.put(javax.xml.transform.OutputKeys.INDENT, "yes");
        packager.createFile(this.dir + this.title + File.separator + "META-INF", "container.xml", builder.asString(prop));
    }

    private String coverContent(String coverFile) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "  <head>\n"
                + "    <meta http-equiv=\"Content-Type\" content=\"application/xhtml+xml; charset=utf-8\"></meta>\n"
                + "    <title>Ok&#322;adka</title>\n"
                + "    <style type=\"text/css\"> img { max-width: 100%; } </style>\n"
                + "  </head>\n"
                + "  <body style=\"oeb-column-number: 1;\">\n"
                + "    <div id=\"cover-image\">\n"
                + "      <img alt=\"Ok&#322;adka\" src=\""
                + coverFile
                + "\"></img>\n"
                + "    </div>\n"
                + "  </body>\n"
                + "</html>";
    }

    private void addImages() {
        for (BufferedImage bufferedImage : images) {
            packager.createImage(this.dir + this.title + File.separator + "OEBPS", "image" + images.indexOf(bufferedImage) + ".jpg", bufferedImage);
        }
    }

    private void addChapters() {
        for (String chapter : chapters) {
            packager.createFile(this.dir + this.title + File.separator + "OEBPS", "chapter" + chapters.indexOf(chapter) + ".html", chapter);
        }
    }

    private void addFonts() {
        for (InputStream font : fonts) {
            packager.createFile(this.dir + this.title + File.separator + "OEBPS", "font" + fonts.indexOf(font) + ".ttf", font);
        }
    }

    @Override
    public void removeChapter(int index) {
        this.chapters.remove(index);
    }

    @Override
    public void removeImage(int index) {
        this.images.remove(index);
    }

    @Override
    public void removeIdentifier(String type) {
        this.identifiers.remove(type);
    }

    @Override
    public void addFont(InputStream in) {
        this.fonts.add(in);
    }

    @Override
    public void removeFont(int index) {
        this.fonts.remove(index);
    }

    private void genTocNcx() {
        try {
            XMLBuilder builder = XMLBuilder.create("ncx", "http://www.daisy.org/z3986/2005/ncx/")
                    .a("version", "2005-1")
                    .e("head")
                    .e("meta").a("name", "dtb:uid").a("content", uid).up()
                    .e("meta").a("name", "dtb:depth").a("content", "1").up()
                    .e("meta").a("name", "dtb:totalPageCount").a("content", "0").up()
                    .e("meta").a("name", "dtb:maxPageNumber").a("content", "0").up()
                    .up()
                    .e("docTitle")
                    .e("text").t(title)
                    .up()
                    .up()
                    .e("navMap");

            if (this.cover != null) {

                builder.e("navPoint").a("id", "navPoint-0").a("playOrder", "0")
                        .e("navLabel")
                        .e("text").t("Okladka").up()
                        .up()
                        .e("content").a("src", "cover.html");
            }

            for (int i = 0; i < chapters.size(); i++) {

                builder.e("navPoint").a("id", "navPoint-" + (i + 1)).a("playOrder", "" + (i + 1))
                        .e("navLabel")
                        .e("text").t("Chapter " + i).up()
                        .up()
                        .e("content").a("src", "chapter" + i + ".html");

            }

            packager.createFile(this.dir + this.title + File.separator + "OEBPS", "toc.ncx", builder.asString());

        } catch (ParserConfigurationException | FactoryConfigurationError | TransformerException ex) {
            Logger.getLogger(Creator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void genContentOpf() {

        String content = "<?xml version='1.0' encoding='utf-8'?>\n"
                + "<package xmlns=\"http://www.idpf.org/2007/opf\"\n"
                + "            xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
                + "            unique-identifier=\"bookid\" version=\"2.0\">";

        String metadata = "<metadata>";

        metadata += "<dc:title>" + this.title + "</dc:title>";
        metadata += "<dc:language>" + this.language + "</dc:language>";

        boolean first = true;
        for (Map.Entry<String, String> entry : identifiers.entrySet()) {
            String string1 = entry.getKey();
            String string2 = entry.getValue();

            if (first) {
                metadata += "<dc:identifier id=\"bookid\" xmlns:opf=\"http://www.idpf.org/2007/opf\" opf:scheme=\"" + string1 + "\">" + string2 + "</dc:identifier>";
                uid = string2;
                first = false;
            } else {
                metadata += "<dc:identifier xmlns:opf=\"http://www.idpf.org/2007/opf\" opf:scheme=\"" + string1 + "\">" + string2 + "</dc:identifier>";
            }
        }

        metadata += "<meta name=\"cover\" content=\"cover-image\" />";

        metadata += "</metadata>";

        String manifest = "<manifest>";
        manifest += "<item id=\"ncx\" href=\"toc.ncx\" media-type=\"application/x-dtbncx+xml\"/>";

        if (this.cover != null) {
            manifest += "<item id=\"cover\" href=\"cover.html\" media-type=\"application/xhtml+xml\"/>";
        }

        for (int i = 0; i < chapters.size(); i++) {
            manifest += "<item id=\"chapter" + i + "\" href=\"chapter" + i + ".html\" media-type=\"application/xhtml+xml\"/>";
        }

        if (this.cover != null) {
            manifest += "<item id=\"cover-image\" href=\"cover.jpg\" media-type=\"image/jpeg\"/>";
        }

        for (int i = 0; i < images.size(); i++) {
            manifest += "<item id=\"image" + i + "\" href=\"image" + i + ".jpg\" media-type=\"application/xhtml+xml\"/>";
        }

        if (this.style != null) {
            manifest += "<item id=\"css\" href=\"style.css\" media-type=\"text/css\"/>";
        }

        for (int i = 0; i < fonts.size(); i++) {
            manifest += "<item id=\"font" + i + "\" href=\"font" + i + ".ttf\" media-type=\"application/x-font-ttf\"/>";
        }

        manifest += "</manifest>";

        String spine = "<spine toc=\"ncx\">";
        spine += "<itemref idref=\"cover\" linear=\"no\"/>";
        for (int i = 0; i < chapters.size(); i++) {
            spine += "<itemref idref=\"chapter" + i + "\"/>";
        }
        spine += "</spine>";

        String guide = "<guide><reference href=\"cover.html\" type=\"cover\" title=\"Cover\"/></guide>";

        content += metadata + manifest + spine + guide + "</package>";

        this.packager.createFile(this.dir + this.title + File.separator + "OEBPS", "content.opf", content);
    }

    @Override
    public void createEpub(boolean keepFiles, boolean epubFile) {

        try {
            writeMimetypeAndContainerFile();
        } catch (IOException | ParserConfigurationException | TransformerException ex) {
            Logger.getLogger(Creator.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (this.cover != null) {
            packager.createImage(this.dir + this.title + File.separator + "OEBPS", "cover.jpg", this.cover);
            packager.createFile(this.dir + this.title + File.separator + "OEBPS", "cover.html", coverContent("cover.jpg"));
        }

        if (this.style != null) {
            packager.createFile(this.dir + this.title + File.separator + "OEBPS", "style.css", this.style);
        }

        addImages();
        addChapters();
        addFonts();
        genContentOpf();
        genTocNcx();

        if (epubFile) {
            packager.pack(this.dir + this.title, this.dir + this.title + ".epub");
        }

        if (!keepFiles) {
            packager.deleteFile(this.dir + this.title);
        }
    }

    @Override
    public void setDir(String dir) {
        this.dir = dir + File.separator;
    }

    @Override
    public void clear() {

        title = null;
        language = null;
        identifiers = new HashMap<>();

        cover = null;
        style = null;

        chapters = new ArrayList<>();
        images = new ArrayList<>();
        fonts = new ArrayList<>();

        uid = null;

    }

    @Override
    public void addChapterBody(String content) {
        String a = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\"\n"
                + "  \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "<head>\n"
                + "  <title>Unknown</title>\n"
                + "  <link href=\"style.css\" rel=\"stylesheet\" type=\"text/css\" />\n"
                + "</head>\n"
                + "<body>";
        
        String b = "</body>\n</html>";
        chapters.add(a + content + b);
    }
}
