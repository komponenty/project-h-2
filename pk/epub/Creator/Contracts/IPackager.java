package pk.epub.Creator.Contracts;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 *
 * @author Artur
 */
public interface IPackager {
    void createFile(String directory, String fileName, String content);
    void createFile(String directory, String fileName, InputStream in);
    void createImage(String directory, String fileName, BufferedImage image);
    void deleteFile(String fileName);
    void pack(String source, String dest);
}
