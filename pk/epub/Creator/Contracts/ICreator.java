package pk.epub.Creator.Contracts;

import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 *
 * @author Artur
 */
public interface ICreator {
    void setTitle(String title);
    void setLanguage(String language);
    void addIdentifier(String type, String value);
    void removeIdentifier(String type);
    void addChapter(String content);
    void addChapterBody(String content);
    void removeChapter(int index);
    void addImage(BufferedImage image);
    void removeImage(int index);
    void addFont(InputStream in);
    void removeFont(int index);
    void setCover(BufferedImage cover);
    void setStyle(String style);
    void createEpub();
    void createEpub(boolean keepFiles, boolean epubFile);
    void setDir(String dir);
    void clear();
    IPackager getPackager();
}