package pk.epub;

import java.awt.EventQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pk.epub.Main.Contracts.IMain;
import pk.epub.Main.Main;

public class Manager {
    
    public static void main(String[] args) {
        final ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
        
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                IMain main = ctx.getBean("main", IMain.class);
            }
        });
        
        
    }
    
}
