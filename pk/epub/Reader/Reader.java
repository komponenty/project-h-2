package pk.epub.Reader;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//do xml
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import pk.epub.Reader.Contracts.IReader;

/**
 *
 * @author Patryk
 */
public class Reader implements IReader, Serializable {

    @Override
    public List<String> getPages() {
        return pages;
    }

    @Override
    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public int getCounter() {
        return counter;
    }

    private List<String> pages;
    private int counter;
    private String Container;

    @Override
    public String getContainer() {
        return Container;
    }

    @Override
    public void Read(String path) {
        try {
            pages = new ArrayList<>();
            counter = 0;
            
            //przeglądanie META-INF container.xml
            File fXmlFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
            
            NodeList nList = doc.getElementsByTagName("rootfile");
            
            String container = null;
            
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) nNode;
                    String value = eElement.getAttribute("full-path");
                    container = value;
                }
            }
            //by dziaiało zawsze
            Container = container.replace("/", File.separator);
            
            //przeglądanie content.opf
            String path2 = path.substring(0, path.length() - "META-INF".length() - "container.xml".length() - 1);
            path2 = path2 + Container;
            
            File fXmlFile2 = new File(path2);
            DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile2);
            
            XPath xPath = XPathFactory.newInstance().newXPath();
            doc.getDocumentElement().normalize();
            
            NodeList elementsByTagName = doc.getElementsByTagName("itemref");
            
            if (elementsByTagName.getLength() != 0) {
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    Node itemref = elementsByTagName.item(i);
                    NamedNodeMap attrs = itemref.getAttributes();
                    Node namedItem1 = attrs.getNamedItem("idref");
                    String idref = namedItem1.getNodeValue();
                    String exp = "package/manifest/item[@id='" + idref + "']";
                    
                    Node node = (Node) xPath.compile(exp).evaluate(doc, XPathConstants.NODE);
                    NamedNodeMap attributes = node.getAttributes();
                    Node namedItem2 = attributes.getNamedItem("href");
                    pages.add(namedItem2.getNodeValue());
                    counter++;
                }
            } //czasami spotyka ns0 przy znacznikach
            else {
                elementsByTagName = doc.getElementsByTagName("ns0:itemref");
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    Node itemref = elementsByTagName.item(i);
                    NamedNodeMap attrs = itemref.getAttributes();
                    Node namedItem1 = attrs.getNamedItem("idref");
                    String idref = namedItem1.getNodeValue();
                    String exp = "package/manifest/item[@id='" + idref + "']";
                    
                    Node node = (Node) xPath.compile(exp).evaluate(doc, XPathConstants.NODE);
                    NamedNodeMap attributes = node.getAttributes();
                    Node namedItem2 = attributes.getNamedItem("href");
                    pages.add(namedItem2.getNodeValue());
                    counter++;
                }
            }
            
            int helper = Container.indexOf(File.separator);
            
            if (helper > 0) {
                
                Container = Container.substring(0, helper);
                if (Container == null) {
                } else {
                    Container = Container + File.separator;
                }
            }
            if (helper <= 0) {
                helper = 0;
                Container = Container.substring(0, helper);
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public File NextPage(String Directory) {
        if (counter >= 0 && counter < pages.size() - 1) {
            counter++;
            try {
                this.setCounter(counter);
                this.saveCounter(Directory);
                return new File(Directory + pages.get(counter).toString());
            } catch (Exception ex) {
            }
        }
        return new File(Directory + pages.get(pages.size() - 1).toString());
    }

    @Override
    public File PreviousPage(String Directory) {
        if (counter > 0 && counter <= pages.size()) {
            counter--;
            try {
                this.setCounter(counter);
                this.saveCounter(Directory);
                return new File(Directory + pages.get(counter).toString());
            } catch (Exception ex) {
            }
        }
        return new File(Directory + pages.get(0).toString());
    }
    //dwie nowe metody do przewijania

    @Override
    public File Beginning(String Directory) {
        counter = 0;
        this.setCounter(counter);
        this.saveCounter(Directory);
        return new File(Directory + pages.get(counter).toString());
    }

    @Override
    public File Ending(String Directory) {
        counter = pages.size() - 1;
        this.setCounter(counter);
        this.saveCounter(Directory);
        return new File(Directory + pages.get(counter).toString());
    }

    //serializacja licznika
    @Override
    public void saveCounter(String Directory) {
        try {
            File counter = new File(Directory + File.separator + "licznik.ser");
            if (counter.exists()) {
                counter.delete();
            }
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(counter));
            out.writeInt(this.getCounter());
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int getCounter(String Directory) {
        File counter = new File(Directory + File.separator + "licznik.ser");
        if (counter.exists()) {
            ObjectInputStream in = null;
            try {
                in = new ObjectInputStream(new FileInputStream(counter));
                return in.readInt();
            } catch (IOException ex) {
                Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    in.close();
                } catch (IOException ex) {
                    Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return 0;
    }
}
