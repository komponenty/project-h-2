package pk.epub.Opener.Contracts;

/**
 *
 * @author Patryk
 */
public interface IOpener {
    
    public String getTitle();
    public String getPath();
    public String getPathToContainer();
    public void OpenEpub(String library, String path, String fname); 
    
}
