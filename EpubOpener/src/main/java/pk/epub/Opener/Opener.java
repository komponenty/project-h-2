package pk.epub.Opener;

import java.io.*;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pk.epub.Opener.Contracts.IOpener;

/**
 *
 * @author Patryk
 */
public class Opener implements IOpener {

    private String path;

    @Override
    public String getPath() {
        return path;
    }
    private String pathToContainer;
    private String title;

    @Override
    public String getTitle() {
        return this.title;
    }

    //sciezka do container.xml
    @Override
    public String getPathToContainer() {
        return pathToContainer;
    }

    @Override
    public void OpenEpub(String library, String path, String fname) {
        try {
            //zrob folder w folderze projektu/library/ o wskazanej nazwie
            File makedir = new File(library + File.separator + fname);
            makedir.mkdir();

            //sciezka do folderu potrzebna, aby tam wypakowac epuba
            this.path = makedir.getAbsolutePath();
            
            //dostep do pliku pod podana sciezka path
            ZipFile zipFile = new ZipFile(path);

            //dostep do pliku xml
            //przekaz dalej to, aby potem odnalezc folder z xml
            pathToContainer = this.path + File.separator + "META-INF" + File.separator + "container.xml";

            //otwieranie zawartosci epuba
            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();

                String name = zipEntry.getName();

                //dzieki temu tworzymy nowy folder w miejscu projektu, a w nim wypakowujemy ksiazki
                File file = new File(this.path + File.separator + name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }

                File parent = file.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                InputStream is = zipFile.getInputStream(zipEntry);
                FileOutputStream fos = new FileOutputStream(file);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = is.read(bytes)) >= 0) {
                    fos.write(bytes, 0, length);
                }
                is.close();
                fos.close();
            }
            zipFile.close();

            //przeglądanie META-INF container.xml, aby zdobyc z content.opf tytul ksiazki
            File fXmlFile = new File(pathToContainer);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("rootfile");

            String container = null;

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    String value = eElement.getAttribute("full-path");
                    container = value;
                }
            }
            //by dziaiało zawsze
            String Container = container.replace("/", File.separator);

            //przeglądanie content.opf
            String path2 = pathToContainer.substring(0, pathToContainer.length() - "META-INF".length() - "container.xml".length() - 1);
            path2 = path2 + Container;

            File fXmlFile2 = new File(path2);
            DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder2 = dbFactory2.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile2);

            XPath xPath = XPathFactory.newInstance().newXPath();
            doc.getDocumentElement().normalize();

            NodeList elementsByTagName = doc.getElementsByTagName("dc:title");
            if (elementsByTagName.getLength() != 0) {
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    Node dcTitle = elementsByTagName.item(i);
                    title=dcTitle.getTextContent();
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(Opener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(Opener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
