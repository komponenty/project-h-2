package pk.epub.Creator;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;
import pk.epub.Creator.Contracts.IPackager;

/**
 *
 * @author Artur
 */
public class Packager implements IPackager {

    private String dir;
    private List<String> fileList;

    public void setFileList(List<String> fileList) {
        this.fileList = fileList;
    }

    public Packager() {
        this.fileList = new ArrayList<>();
    }

    @Override
    public void createFile(String directory, String fileName, String content) {
        Writer file = null;
        try {
            file = new FileWriter(directory + File.separator + fileName);
            file.write(content);
            file.close();
        } catch (IOException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void createImage(String directory, String fileName, BufferedImage image) {
        try {
            File out = new File(directory + File.separator + fileName);
            String type = null;
            int i;
            if ((i = fileName.lastIndexOf('.')) > 0) {
                type = fileName.substring(i + 1);
            }
            ImageIO.write(image, type, out);
        } catch (IOException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void pack(String source, String destinationPath) {
        this.dir = source;
        fileList.add(generateZipEntry(new File(this.dir, "mimetype").getPath().toString()));
        try {
            generateFileList(new File(dir));
        } catch (IOException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        }
        zipIt(destinationPath);
        this.fileList = new ArrayList<>();
    }

    @Override
    public void deleteFile(String fileName) {
        File file = new File(fileName);
        try {
            delete(file);
        } catch (Exception ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void delete(File f) throws Exception {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                delete(c);
            }
        }
        if (!f.delete()) {
            throw new Exception("Failed to delete file: " + f);
        }
    }

    @Override
    public void createFile(String directory, String fileName, InputStream in) {
        OutputStream out = null;
        try {
            File file = new File(directory + File.separator + fileName);
            out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while (-1 != (read = in.read(bytes))) {
                out.write(bytes, 0, read);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void zipIt(String zipFile) {
        FileOutputStream fos = null;
        try {
            byte[] buffer = new byte[1024];
            fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String file : this.fileList) {

                ZipEntry ze = new ZipEntry(file);
                if (ze.getName().equalsIgnoreCase("mimetype")) {
                    ze.setMethod(ZipEntry.STORED);
                    ze.setSize(20);
                    ze.setCompressedSize(20);
                    ze.setCrc(0x2CAB616F);
                }
                zos.putNextEntry(ze);

                FileInputStream in = new FileInputStream(this.dir + File.separator + file);

                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }

                in.close();
            }
            zos.closeEntry();
            //remember close it
            zos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Packager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generateFileList(File node) throws IOException {
        
	if(node.isFile() && !(node.getName().equalsIgnoreCase("mimetype"))){
            fileList.add(generateZipEntry(node.getPath().toString()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }

    }

    private String generateZipEntry(String file){
    	return file.substring(this.dir.length()+1, file.length());
    }
}
