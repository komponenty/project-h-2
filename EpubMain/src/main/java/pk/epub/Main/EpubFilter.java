/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.epub.Main;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Artur
 */
public class EpubFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 && i < s.length() - 1) {
            extension = s.substring(i + 1).toLowerCase();
        }        
        if (extension != null) {
            return extension.equals("epub");
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "Electronic publication (.epub)";
    }

}
