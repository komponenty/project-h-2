package pk.epub.Main;

import pk.epub.Main.Contracts.IMixin;

/**
 *
 * @author Artur
 */
public class Mixin implements IMixin {

    @Override
    public void secretEpub() {
        System.out.println("Resolution 8: Re-establishment of Ad Hoc Group 4 on EPUB. SC 34 re-establishes Ad Hoc Group 4 on EPUB of IDPF* with the following terms of reference: – to prepare the creation of a Joint Working Group (JWG) for EPUB (and possibly other related topics) under JTC 1/SC 34 with ISO TC 46 and IEC TC 100 /TA 10 involved. SC 34 notes that EPUB 3 will be submitted as a Draft Technical Specification by the Korean National Body via the JTC 1 fast-track procedure and it will be assigned to the SC 34/JWG when approved.");
    }
    
}
