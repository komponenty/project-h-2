package pk.epub.Library;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import pk.epub.Library.Contracts.ILibrary;

/**
 *
 * @author Artur
 */
public class LibraryView extends JPanel {

    private ILibrary library;

    public ILibrary getLibrary() {
        return library;
    }

    public void setLibrary(ILibrary library) {
        this.library = library;
    }
    
    public LibraryView(ILibrary library) {
        this.library = library;
        initComponents();
        jTable1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public JTable getjTable1() {
        return jTable1;
    }

    public void setjTable1(JTable jTable1) {
        this.jTable1 = jTable1;
    }   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jTable1.setModel(this.library.getModel());
        jScrollPane1.setViewportView(jTable1);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
