package pk.epub.Library;

import glowne.BazaDanychComponent.Contract.IBaza;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JTable;
import javax.swing.event.ListSelectionListener;
import pk.epub.Library.Contracts.ILibrary;

/**
 *
 * @author Artur
 */
public class Library implements ILibrary {

    private LibraryView view;
    private LibraryModel model;
    
    private final String libraryDir;

    @Override
    public String getLibraryDir() {
        return libraryDir;
    }

    public Library(IBaza db, String dbFileName, String libraryDir) {
        this.model = new LibraryModel(db, dbFileName);
        this.view = new LibraryView(this);
        this.libraryDir = libraryDir;
        File dir = new File(libraryDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    @Override
    public LibraryView getView() {
        return view;
    }

    @Override
    public LibraryModel getModel() {
        return model;
    }

    public void setView(LibraryView view) {
        this.view = view;
    }

    public void setModel(LibraryModel model) {
        this.model = model;
    }

    @Override
    public String[] getSelectedEpub() {
        JTable table = view.getjTable1();
        if (table.getSelectedRow() != -1 && table.getSelectedRow() < table.getRowCount()) {
            return model.getRow(view.getjTable1().getSelectedRow());
        }
        return new String[] { "", "" };
    }

    @Override
    public void addNewEpub(String name, String path) {
        model.addRow(name, path);
    }

    @Override
    public void removeSelectedEpub() {
        JTable table = view.getjTable1();
        if (table.getSelectedRow() != -1 && table.getSelectedRow() < table.getRowCount()) {
            model.removeRow(view.getjTable1().getSelectedRow());
        }
    }

    @Override
    public void editSelectedEpub(String name, String path) {
        JTable table = view.getjTable1();
        if (table.getSelectedRow() != -1) {
            model.editRow(view.getjTable1().getSelectedRow(), name, path);
        }
    }

    @Override
    public void addSelectionListener(ListSelectionListener lsl) {
        view.getjTable1().getSelectionModel().addListSelectionListener(lsl);
    }

    @Override
    public void addMouseListener(MouseListener ml) {
        view.getjTable1().addMouseListener(ml);
    }
}
