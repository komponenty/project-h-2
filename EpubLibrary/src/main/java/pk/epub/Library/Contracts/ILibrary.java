package pk.epub.Library.Contracts;

import java.awt.event.MouseListener;
import javax.swing.event.ListSelectionListener;
import pk.epub.Library.LibraryModel;
import pk.epub.Library.LibraryView;

/**
 *
 * @author Artur
 */
public interface ILibrary {
    LibraryView getView();
    LibraryModel getModel();
    String[] getSelectedEpub();
    void addNewEpub(String name, String path);
    void removeSelectedEpub();
    void editSelectedEpub(String name, String path);
    void addSelectionListener(ListSelectionListener lsl);
    void addMouseListener(MouseListener ml);
    String getLibraryDir();
}
