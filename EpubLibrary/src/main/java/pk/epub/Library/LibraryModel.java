package pk.epub.Library;

import glowne.BazaDanychComponent.Contract.IBaza;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Artur
 */
public class LibraryModel extends AbstractTableModel {

    private final IBaza database;
    private final String dbFileName;

    private final String[] columnNames = {"Name", "Path"};

    private List<Epub> data = new ArrayList<>();

    public LibraryModel(IBaza db, String dbFileName) {
        
        this.database = db;
        this.dbFileName = dbFileName;
        this.data = selectAll();
        
    }
    
    public void ref() {
        this.data = selectAll();
    }

    @Override
    public int getRowCount() {
        
        return data.size();
        
    }

    @Override
    public int getColumnCount() {
        
        return columnNames.length;
        
    }

    @Override
    public String getColumnName(int column) {
        
        return columnNames[column];
        
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        if (columnIndex == 0) {
            return data.get(rowIndex).getName();
        } else {
            return data.get(rowIndex).getPath();
        }
        
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        
        if (col == 0) {
            data.get(row).setName((String) value);
        } else {
            data.get(row).setPath((String) value);
        }
        fireTableCellUpdated(row, col);
        
    }

    private List<Epub> selectAll() {
        
        List<Epub> result = database.znajdzObiekt(dbFileName, false, new Epub());
        return result;
        
    }

    public void addRow(String name, String path) {
        
        Epub epub = new Epub(name, path);
        boolean result = database.dodajObiekt(dbFileName, false, epub);
        if (result) {
            this.data.add(epub);
            fireTableRowsInserted(0, getRowCount());
        }
        
    }

    public void removeRow(int row) {
        
        Epub epub = this.data.get(row);
        database.usunObiekt(dbFileName, false, epub);
        this.data.remove(epub);
        fireTableDataChanged();
        
    }
    
    public void editRow(int row, String name, String path) {
        
        Epub epub = this.data.get(row);
        Epub newEpub = new Epub(name, path);
        boolean result = database.edytujObiekt(dbFileName, false, epub, newEpub);
        if (result) {
            this.data.set(row, newEpub);
            fireTableRowsUpdated(0, getRowCount());
        }
        
    }
    
    public String[] getRow(int row) {
        
        Epub epub = this.data.get(row);
        String[] tab = new String[] { epub.getName(), epub.getPath() };
        return tab;
        
    }
}
